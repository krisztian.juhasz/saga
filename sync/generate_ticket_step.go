package sync

import "fmt"

type GenerateTicketStep struct {
}

func (receiver GenerateTicketStep) Run() error {
	fmt.Println("Setting checkout status to generate ticket")
	fmt.Println("Start generate tickets")
	fmt.Println("Finish generate tickets")
	fmt.Println("Update checkout status")
	return nil
}

func (receiver GenerateTicketStep) Compensate() {
	fmt.Println("Set checkout to stuck")
	fmt.Println("Set cart to failed")
}
