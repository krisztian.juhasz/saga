package async

import (
	"errors"
	"fmt"
)

type SendMailStep struct {
}

func (receiver SendMailStep) Run() error {
	fmt.Println("Setting checkout status to sending mail")
	fmt.Println("Start sending mail")
	go func() { c <- "SendMailRequest" }()
	return nil
}

func (receiver SendMailStep) GetResponseType() string {
	return "SendMailResponse"
}

func (receiver SendMailStep) Continue(msg string) error {
	return errors.New("send error")
	//fmt.Println("Mail sent")
	//fmt.Println("Update checkout status")
	//return nil
}

func (receiver SendMailStep) Compensate() {
	fmt.Println("Set checkout to stuck")
	fmt.Println("Set cart to failed")
}
