package async

import "time"

var c = make(chan string)

type Handler struct {
	Saga *Saga
}

func (h Handler) HandleRequest() {
	for {
		switch <-c {
		case "TicketGenerateRequest":
			{
				time.Sleep(5 * time.Second)
				h.Saga.Continue("TicketGeneratedResponse", "")
			}
		case "SendMailRequest":
			{
				time.Sleep(2 * time.Second)
				h.Saga.Continue("SendMailResponse", "")
			}

		}
	}
}
