package async

import "fmt"

type GenerateTicketStep struct {
}

func (receiver GenerateTicketStep) Run() error {
	fmt.Println("Setting checkout status to generate ticket")
	fmt.Println("Start generate tickets")
	go func() { c <- "TicketGenerateRequest" }()
	return nil
}

func (receiver GenerateTicketStep) GetResponseType() string {
	return "TicketGeneratedResponse"
}

func (receiver GenerateTicketStep) Continue(msg string) error {
	fmt.Println("Finish generate tickets")
	fmt.Println("Update checkout status")
	return nil
}

func (receiver GenerateTicketStep) Compensate() {
	fmt.Println("Set checkout to stuck")
	fmt.Println("Set cart to failed")
}
