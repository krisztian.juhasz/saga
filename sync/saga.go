package sync

import "fmt"

type Saga struct {
	steps []SagaStep
}

type SagaStep interface {
	Run() error
	Compensate()
}

func (s *Saga) AddStep(step SagaStep) {
	s.steps = append(s.steps, step)
}

func (s *Saga) Start() {
	fmt.Println("Start checkout saga")

	for _, step := range s.steps {
		err := step.Run()
		if err != nil {
			fmt.Println(err)
			step.Compensate()
			return
		}
	}

	fmt.Println("Saga succeed")
}
