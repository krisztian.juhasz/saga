package sync

import (
	"errors"
	"fmt"
)

type SendMailStep struct {
}

func (receiver SendMailStep) Run() error {
	fmt.Println("Setting checkout status to sending mail")
	fmt.Println("Start sending mail")
	return errors.New("send error")
	//fmt.Println("Mail sent")
	//fmt.Println("Update checkout status")
	//return nil
}

func (receiver SendMailStep) Compensate() {
	fmt.Println("Set checkout to stuck")
	fmt.Println("Set cart to failed")
}
