package async

import "fmt"

type Saga struct {
	steps []SagaStep
}

func (s *Saga) AddStep(step SagaStep) {
	s.steps = append(s.steps, step)
}

func (s *Saga) Start() {
	fmt.Println("Start checkout saga")
	if len(s.steps) == 0 {
		fmt.Println("No steps for checkout")
	}
	err := s.steps[0].Run()
	if err != nil {
		fmt.Println(err)
	}
}

func (s *Saga) Continue(cmd string, msg string) {
	for i, step := range s.steps {
		if step.GetResponseType() == cmd {

			err := step.Continue(msg)
			if err != nil {
				step.Compensate()
			}

			if s.isLastStep(i) {
				fmt.Println("Saga succeed")
				break
			}

			err = s.steps[i+1].Run()
			if err != nil {
				fmt.Println(err)
			}
			break
		}
	}
}

func (s *Saga) isLastStep(position int) bool {
	return len(s.steps)-1 == position

}

type SagaStep interface {
	Run() error
	GetResponseType() string
	Continue(msg string) error
	Compensate()
}
