package main

import (
	"saga/async"
	"saga/sync"
	"time"
)

func main() {
	//startSyncSaga()
	startAsyncSaga()
}

func startSyncSaga() {
	checkoutSaga := sync.Saga{}
	checkoutSaga.AddStep(sync.GenerateTicketStep{})
	checkoutSaga.AddStep(sync.SendMailStep{})
	checkoutSaga.Start()
}

func startAsyncSaga() {
	checkoutSaga := async.Saga{}

	h := async.Handler{Saga: &checkoutSaga}
	go h.HandleRequest()

	checkoutSaga.AddStep(async.GenerateTicketStep{})
	checkoutSaga.AddStep(async.SendMailStep{})
	checkoutSaga.Start()
	time.Sleep(20 * time.Second)
}
